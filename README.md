# Overview

Nodejs module - adapted version of [`eNUMERATION.js` library](https://bitbucket.org/workslon/ontojs/src/8367a54d7dbdc4330a750dfdfbbd9a3e27e17867/src/eNUMERATION.js) written by Prof. Gerd Wagner, BTU Cottbus

You can find its source code [here](https://bitbucket.org/workslon/enumeration/src/97fc536e81496ffcf5ca8720983b58e3903142c0/index.js)

# Install

## ssh

```
npm install git+ssh://git@bitbucket.org/workslon/enumeration.git --save
```

## https

```
npm install https://git@bitbucket.org/workslon/enumeration.git --save
```

# Usage

## CommonJS

```javascript
var eNUMERATION = require('eNUMERATION');

// ...
```

## ES6 Modules

```javascript
import eNUMERATION from 'eNUMERATION';

// ...
```